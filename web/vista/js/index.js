$(document).ready(function() {
  cargarComboCountry('#cbocountry', 'seleccione');
});

$('#cbocountry').change(function() {
  $('#cbostate').val([]);
  $('#cbocity').val([]);

  var id_country = $('#cbocountry').val();
  cargarComboState('#cbostate', 'seleccione', id_country);
});

$('#cbostate').change(function() {
  $('#cbocity').val([]);

  var id_country = $('#cbocountry').val();
  var id_state = $('#cbostate').val();
  cargarComboCity('#cbocity', 'seleccione', id_country, id_state);
});

$('#frmregistro').submit(function(evento) {
  evento.preventDefault();

  var ruta = DIRECCION_WS + 'usuario.registrar.php';
  var tipo = $('#cbotipo').val();
  var nombres = $('#txtnombre').val();
  var apellidos = $('#txtapellidos').val();
  var fecha_nacimiento = $('#txtfechanacimiento').val();
  var correo = $('#txtcorreo').val();
  var telefono1 = $('#txttelefono1').val();
  var telefono2 = $('#txttelefono2').val();
  var id_country = $('#cbocountry').val();
  var id_state = $('#cbostate').val();
  var id_city = $('#cbocity').val();

  swal({
    title: '¿Desea Registrar?',
    text: 'se agregará una nueva caja!',
    showCancelButton: true,
    confirmButtonClass: 'btn btn-confirm mt-2',
    cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
    confirmButtonText: 'registrar',
    cancelButtonText: 'cancelar',
    imageUrl: '../img/sweetalert/pregunta.png'
  }).then(function() {
    $.post(
      ruta,
      {
        tipo: tipo,
        nombres: nombres,
        apellidos: apellidos,
        fecha_nacimiento: fecha_nacimiento,
        correo: correo,
        telefono1: telefono1,
        telefono2: telefono2,
        id_country: id_country,
        id_state: id_state,
        id_city: id_city
      },
      function() {}
    )
      .done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
          swal({
            title: 'EXITO!',
            text: datosJSON.mensaje,
            type: 'success',
            confirmButtonClass: 'btn btn-confirm mt-2'
          });
        } else {
          swal('Mensaje del sistema', resultado, 'warning');
        }
      })
      .fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal('Error', datosJSON.mensaje, 'error');
      });
  });
});
