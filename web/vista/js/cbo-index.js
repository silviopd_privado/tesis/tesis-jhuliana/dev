function cargarComboCountry(p_nombreCombo, p_tipo) {
  var ruta = DIRECCION_WS + "country.listar.php";

  $.post(ruta, function () {}).done(function (resultado) {
    var datosJSON = resultado;
    if (datosJSON.estado === 200) {
      var html = "";
      if (p_tipo === "seleccione") {
        html += '<option value="">Seleccione</option>';
      } else {
        html += '<option value="0">Todos</option>';
      }

      $.each(datosJSON.datos, function (i, item) {
        html += '<option value="' + item.id_country + '">' + item.name + '</option>';
      });

      $(p_nombreCombo).html(html);
    } else {
      swal("Mensaje del sistema", resultado, "warning");
    }
  }).fail(function (error) {
    var datosJSON = $.parseJSON(error.responseText);
    swal("Error", datosJSON.mensaje, "error");
  })
}

function cargarComboState(p_nombreCombo, p_tipo, p_id_country) {
  var ruta = DIRECCION_WS + "state.listar.php";

  $.post(ruta, {
    id_country: p_id_country
  }, function () {}).done(function (resultado) {
    var datosJSON = resultado;
    if (datosJSON.estado === 200) {
      var html = "";
      if (p_tipo === "seleccione") {
        html += '<option value="">Seleccione</option>';
      } else {
        html += '<option value="0">Todos</option>';
      }

      $.each(datosJSON.datos, function (i, item) {
        html += '<option value="' + item.id_state + '">' + item.name + '</option>';
      });

      $(p_nombreCombo).html(html);
    } else {
      swal("Mensaje del sistema", resultado, "warning");
    }
  }).fail(function (error) {
    var datosJSON = $.parseJSON(error.responseText);
    swal("Error", datosJSON.mensaje, "error");
  })
}

function cargarComboCity(p_nombreCombo, p_tipo, p_id_country,p_id_state) {
  var ruta = DIRECCION_WS + "city.listar.php";

  $.post(ruta, {id_country: p_id_country,id_state:p_id_state}, function () {
  }).done(function (resultado) {
      var datosJSON = resultado;
      if (datosJSON.estado === 200) {
          var html = "";
          if (p_tipo === "seleccione") {
              html += '<option value="">Seleccione</option>';
          } else {
              html += '<option value="0">Todos</option>';
          }

          $.each(datosJSON.datos, function (i, item) {
              html += '<option value="' + item.id_city + '">' + item.name + '</option>';
          });

          $(p_nombreCombo).html(html);
      } else {
          swal("Mensaje del sistema", resultado, "warning");
      }
  }).fail(function (error) {
      var datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
  })
}