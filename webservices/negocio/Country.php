<?php

require_once '../datos/Conexion.clase.php';

class Country extends Conexion {

    //put your code here
    private $id_country, $shortname, $name;

    function getId_country()
    {
        return $this->id_country;
    }

    function getShortname()
    {
        return $this->shortname;
    }

    function getName()
    {
        return $this->name;
    }

    function setId_country($id_country)
    {
        $this->id_country = $id_country;
    }

    function setShortname($shortname)
    {
        $this->shortname = $shortname;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    public function listar()
    {
        try {
            $sql = "select * from country order by name";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
