<?php

require_once '../datos/Conexion.clase.php';

class City extends Conexion{

    //put your code here
    private $id_city, $name, $id_country, $id_state;

    function getId_city()
    {
        return $this->id_city;
    }

    function getName()
    {
        return $this->name;
    }

    function getId_country()
    {
        return $this->id_country;
    }

    function getId_state()
    {
        return $this->id_state;
    }

    function setId_city($id_city)
    {
        $this->id_city = $id_city;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function setId_country($id_country)
    {
        $this->id_country = $id_country;
    }

    function setId_state($id_state)
    {
        $this->id_state = $id_state;
    }

    public function listar($id_country, $id_state)
    {
        try {
            $sql = "select * from city where id_country = :p_id_country and id_state = :p_id_state order by name";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_country", $id_country);
            $sentencia->bindParam(":p_id_state", $id_state);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
