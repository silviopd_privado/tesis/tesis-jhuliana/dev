<?php

require_once '../datos/Conexion.clase.php';

class State extends Conexion{
    //put your code here
    private $id_state,$name,$id_country;
    
    function getId_state()
    {
        return $this->id_state;
    }

    function getName()
    {
        return $this->name;
    }

    function getId_country()
    {
        return $this->id_country;
    }

    function setId_state($id_state)
    {
        $this->id_state = $id_state;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function setId_country($id_country)
    {
        $this->id_country = $id_country;
    }

    public function listar($id_country)
    {
        try {
            $sql = "select * from state where id_country = :p_id_country order by name";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_country", $id_country);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
