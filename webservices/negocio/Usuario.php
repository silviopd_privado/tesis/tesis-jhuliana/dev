<?php

require_once '../datos/Conexion.clase.php';

class Usuario extends Conexion {

    private $tipo,$id_alumno, $nombres, $apellidos, $correo, $telefono1, $telefono2, $fecha_nacimiento, $id_city, $id_state, $id_country, $estado;

    function getTipo()
    {
        return $this->tipo;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }
    
    function getId_alumno()
    {
        return $this->id_alumno;
    }

    function getNombres()
    {
        return $this->nombres;
    }

    function getApellidos()
    {
        return $this->apellidos;
    }

    function getCorreo()
    {
        return $this->correo;
    }

    function getTelefono1()
    {
        return $this->telefono1;
    }

    function getTelefono2()
    {
        return $this->telefono2;
    }

    function getFecha_nacimiento()
    {
        return $this->fecha_nacimiento;
    }

    function getId_city()
    {
        return $this->id_city;
    }

    function getId_state()
    {
        return $this->id_state;
    }

    function getId_country()
    {
        return $this->id_country;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setId_alumno($id_alumno)
    {
        $this->id_alumno = $id_alumno;
    }

    function setNombres($nombres)
    {
        $this->nombres = $nombres;
    }

    function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    function setTelefono1($telefono1)
    {
        $this->telefono1 = $telefono1;
    }

    function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;
    }

    function setFecha_nacimiento($fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    function setId_city($id_city)
    {
        $this->id_city = $id_city;
    }

    function setId_state($id_state)
    {
        $this->id_state = $id_state;
    }

    function setId_country($id_country)
    {
        $this->id_country = $id_country;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function agregar()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL f_usuario_registro(:p_tipo, :p_nombres, :p_apellidos, :p_correo, :p_telefono1, :p_telefono2, :p_fecha_nacimiento, :p_id_city, :p_id_state, :p_id_country) ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_tipo", $this->getTipo());
            $sentencia->bindValue(":p_nombres", $this->getNombres());
            $sentencia->bindValue(":p_apellidos", $this->getApellidos());
            $sentencia->bindValue(":p_correo", $this->getCorreo());
            $sentencia->bindValue(":p_telefono1", $this->getTelefono1());
            $sentencia->bindValue(":p_telefono2", $this->getTelefono2());
            $sentencia->bindValue(":p_fecha_nacimiento", $this->getFecha_nacimiento());
            $sentencia->bindValue(":p_id_city", $this->getId_city());
            $sentencia->bindValue(":p_id_state", $this->getId_state());
            $sentencia->bindValue(":p_id_country", $this->getId_country());
            $sentencia->execute();

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

}
