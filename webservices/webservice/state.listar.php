<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/State.php';
require_once '../util/funciones/Funciones.clase.php';


if (!isset($_POST["id_country"]))
{
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$id_country = $_POST["id_country"];

try {
    $obj = new State();
    $resultado = $obj->listar($id_country);

    $listastate = array();
    for ($i = 0; $i < count($resultado); $i++)
    {

        $datos = array(
            "id_country" => $resultado[$i]["id_country"],
            "id_state" => $resultado[$i]["id_state"],
            "name" => $resultado[$i]["name"]
        );

        $listastate[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listastate);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}

