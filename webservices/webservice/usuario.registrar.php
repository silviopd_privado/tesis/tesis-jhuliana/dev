<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Usuario.php';
require_once '../util/funciones/Funciones.clase.php';

$tipo = $_POST["tipo"];
$nombres = $_POST["nombres"];
$apellidos = $_POST["apellidos"];
$correo = $_POST["correo"];
$telefono1 = $_POST["telefono1"];
$telefono2 = $_POST["telefono2"];
$fecha_nacimiento = str_replace('/', '-', $_POST["fecha_nacimiento"]);
$id_city = $_POST["id_city"];
$id_state = $_POST["id_state"];
$id_country = $_POST["id_country"];

try {
    $obj = new Usuario();
    $obj->setTipo($tipo);
    $obj->setNombres($nombres);
    $obj->setApellidos($apellidos);
    $obj->setCorreo($correo);
    $obj->setTelefono1($telefono1);
    $obj->setTelefono2($telefono2);
    $obj->setFecha_nacimiento(date('Y-m-d', strtotime($fecha_nacimiento)));
    $obj->setId_city($id_city);
    $obj->setId_state($id_state);
    $obj->setId_country($id_country);
    $resultado = $obj->agregar();

    if ($resultado)
    {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    }
    else
    {
        Funciones::imprimeJSON(500, "No se pudo Registrar", "Error...");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
