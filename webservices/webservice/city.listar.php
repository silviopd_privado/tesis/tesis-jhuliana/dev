<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/City.php';
require_once '../util/funciones/Funciones.clase.php';


if (!isset($_POST["id_country"]) || !isset($_POST["id_state"]))
{
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$id_country = $_POST["id_country"];
$id_state = $_POST["id_state"];

try {
    $obj = new City();
    $resultado = $obj->listar($id_country, $id_state);

    $listacity = array();
    for ($i = 0; $i < count($resultado); $i++)
    {

        $datos = array(
            "id_country" => $resultado[$i]["id_country"],
            "id_state" => $resultado[$i]["id_state"],
            "id_city" => $resultado[$i]["id_city"],
            "name" => $resultado[$i]["name"]
        );

        $listacity[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacity);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
