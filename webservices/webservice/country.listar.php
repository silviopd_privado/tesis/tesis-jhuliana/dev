<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Country.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Country();
    $resultado = $obj->listar();

    $listacountry = array();
    for ($i = 0; $i < count($resultado); $i++)
    {

        $datos = array(
            "id_country" => $resultado[$i]["id_country"],
            "name" => $resultado[$i]["name"]
        );

        $listacountry[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacountry);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
